<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/clientes/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_cli" id="id_cli"
   value="<?php echo $cliente->id_cli; ?>">
    <br>
    <br>
    <label for="">PAQUETE</label>
    <select class="form-control" name="fk_id_paq" id="fk_id_paq"
    required >
        <option value="">--Seleccione un paquete--</option>
        <?php if ($listadoPaquetes): ?>
          <?php foreach ($listadoPaquetes->result() as $paqueteTemporal): ?>
              <option value="<?php echo $paqueteTemporal->id_paq; ?>">
                <?php echo $paqueteTemporal->nombre_paq; ?>
              </option>
          <?php endforeach; ?>
        <?php endif; ?>
    </select>

    <br>
    <br>
    <label for="">IDENTIFICACIÓN</label>
    <input class="form-control"
    value="<?php echo $cliente->identificacion_cli; ?>"
    type="number" name="identificacion_cli"
    id="identificacion_cli"
    placeholder="Por favor Ingrese la Identificacion">
    <br>
    <br>
    <label for="">APELLIDO</label>
    <input class="form-control"
    value="<?php echo $cliente->apellido_cli; ?>"
    type="text" name="apellido_cli"
    id="apellido_cli"
    placeholder="Por favor Ingrese el apellido">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control" value="<?php echo $cliente->nombre_cli; ?>"   type="text" name="nombre_cli" id="nombre_cli" placeholder="Por favor Ingrese el nombre">
    <br>
    <br>
    <label for="">TELEFONO</label>
    <input class="form-control" value="<?php echo $cliente->telefono_cli; ?>"    type="number" name="telefono_cli" id="telefono_cli" placeholder="Por favor Ingrese el telefono">
  <br>
    <br>
    <label for="">CORREO ELECTRÓNICO</label>
    <input class="form-control" value="<?php echo $cliente->email_cli; ?>"    type="email" name="email_cli" id="email_cli" placeholder="Por favor Ingrese el correo">
    <br>
    <br>
    <label for="">SEXO</label>
    <select class="form-control" name="sexo_cli" id="sexo_cli" 
        <option value="<?php echo $cliente->sexo_cli; ?>"></option>
        <option value="MASCULINO ">MASCULINO </option>
        <option value="FEMENINO">FEMENINO</option>
    </select>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/clientes/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a>

</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#fk_id_paq")
   .val("<?php echo $cliente->fk_id_paq; ?>");

   $("#estado_cli")
   .val("<?php echo $cliente->estado_paq; ?>");

</script>
