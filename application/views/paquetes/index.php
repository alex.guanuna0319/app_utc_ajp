<br>
<center>
  <h2>LISTADO DE PAQUETES
  </h2>
<center>
    <a href="<?php echo site_url(); ?>/paquetes/nuevo" class="btn btn-primary">
      <i class="fa fa-plus-circle"></i>
      Agregar Nuevo
    </a>
</center>

<?php if ($listadoPaquetes): ?>
  <table class="table" id="tbl-clientes">
        <thead>
            <tr>

              <th class="text-center">ID</th>
              <th class="text-center">FOTO</th>
              <th class="text-center">NOMBRE</th>
              <th class="text-center">TIPO</th>
              <th class="text-center">ORIGEN</th>
              <th class="text-center">DESTINO</th>
              <th class="text-center">FECHA PARTIDA</th>
              <th class="text-center">FECHA RETORNO</th>
              <th class="text-center">DIAS DE VIAJE</th>
              <th class="text-center">PRECIO</th>
              <th class="text-center">DESCRIPCION</th>
              <th class="text-center">OPCIONES</th>

            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoPaquetes->result()
            as $filaTemporal): ?>
                  <tr>
                    <td class="text-center">
                      <?php echo $filaTemporal->id_paq; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($filaTemporal->foto_paq!=""): ?>
                        <img
                        src="<?php echo base_url(); ?>/uploads/paquetes/<?php echo $filaTemporal->foto_paq; ?>"
                        height="80px"
                        width="100px"
                        alt="">
                      <?php else: ?>
                        N/A
                      <?php endif; ?>
                    </td>

                    <td class="text-center">
                      <?php echo $filaTemporal->nombre_paq; ?>
                    </td>
                    <td class="text-center">
                      <?php if ($filaTemporal->tipo_paq=="VIAJE"): ?>
                        <div class="alert alert-success">
                          <?php echo $filaTemporal->tipo_paq; ?>
                        </div>
                      <?php else: ?>
                        <div class="alert alert-danger">
                          <?php echo $filaTemporal->tipo_paq; ?>
                        </div>
                      <?php endif; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->origen_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->destino_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->fecha_partida_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->fecha_retorno_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->dias_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->precio_paq; ?>
                    </td>
                    <td class="text-center">
                        <?php echo $filaTemporal->descripcion_paq; ?>
                    </td>
                    <td class="text-center">
                        <a href="<?php echo site_url(); ?>/paquetes/editar/<?php echo $filaTemporal->id_paq; ?>" class="btn btn-warning">
                          <i class="fa fa-pen"></i>
                        </a>


                          <a href="javascript:void(0)"
                           onclick="confirmarEliminacion('<?php echo $filaTemporal->id_paq; ?>');"
                           class="btn btn-danger">
                            <i class="fa fa-trash"></i>
                          </a>
                    </td>
                  </tr>
            <?php endforeach; ?>
        </tbody>
  </table>
<?php else: ?>
<div class="alert alert-danger">
    <h3>No se encontraron paquetes registrados</h3>
</div>
<?php endif; ?>

<script type="text/javascript">
    function confirmarEliminacion(id_paq){
          iziToast.question({
              timeout: 20000,
              close: false,
              overlay: true,
              displayMode: 'once',
              id: 'question',
              zindex: 999,
              title: 'CONFIRMACIÓN',
              message: '¿Esta seguro de eliminar el cliente de forma pernante?',
              position: 'center',
              buttons: [
                  ['<button><b>SI</b></button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                      window.location.href=
                      "<?php echo site_url(); ?>/paquetes/procesarEliminacion/"+id_paq;

                  }, true],
                  ['<button>NO</button>', function (instance, toast) {

                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                  }],
              ]
          });
    }
</script>

<script type="text/javascript">
    //Deber incorporar botones de EXPORTACION
    $("#tbl-paquetes").DataTable();
</script>










<!--  -->
