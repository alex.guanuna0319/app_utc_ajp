<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form action="<?php echo site_url(); ?>/paquetes/guardarPaquete"
  method="post"
  id="frm_nuevo_paquete"
  enctype="multipart/form-data"
  >

    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"  type="text" name="nombre_paq" id="nombre_paq" placeholder="Por favor ingrese el nombre del paquete">
    <br>
    <br>
    <label for="">TIPO</label>
    <select class="form-control" name="tipo_paq" id="tipo_paq">
        <option value="">--Seleccione--</option>
        <option value="VIAJE">VIAJE</option>
        <option value="CRUCERO">CRUCERO</option>
    </select>
    <br>
    <label for="">ORIGEN</label>
    <input class="form-control"  type="text" name="origen_paq" id="origen_paq" placeholder="Por favor Ingrese el pais de origen">
    <br>
    <br>
    <label for="">DESTINO</label>
    <input class="form-control"  type="text" name="destino_paq" id="destino_paq" placeholder="Por favor Ingrese el pais de destino">
    <br>
    <br>
    <label for="">FECHA PARTIDA</label>
    <input class="form-control"  type="date" name="fecha_partida_paq" id="fecha_partida_paq" placeholder="Por favor Ingrese la fecha de salida">
    <br>
    <br>
    <label for="">FECHA RETORNO</label>
    <input class="form-control"  type="date" name="fecha_retorno_paq" id="fecha_retorno_paq" placeholder="Por favor Ingrese la fecha de retorno">
    <br>
    <br>
    <label for="">DIAS</label>
    <input class="form-control"  type="number" name="dias_paq" id="dias_paq" placeholder="Por favor Ingrese los dias de viaje">

    <br>
    <br>
    <label for="">PRECIO</label>
    <input class="form-control"  type="numbrer" name="precio_paq" id="precio_paq" placeholder="Por favor Ingrese el precio del paquete">

    <br>
    <br>
    <label for="">DESCRIPCION</label>
    <input class="form-control"  type="text" name="descripcion_paq" id="descripcion_paq" placeholder="Por favor Ingrese la descripcion del paquete">

    <br>
    <br>
    <label for="">FOTOGRAFIA</label>
    <input type="file" name="foto_paq"
    accept="image/*"
    id="foto_paq"  value="" >
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/paquetes/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i>
      CANCELAR
    </a>

</form>
</div>
</div>
</div>
<script type="text/javascript">
    $("#frm_nuevo_paquete").validate({
      rules:{
        nombre_paq:{
          required:true
        },
        tipo_paq:{
          required:true

        },
        origen_paq:{
          required:true,
          letras:true
        },
        destino_paq:{
          required:true,
          letras:true
        },
       fecha_partida_paq:{
          required:true
        },
        fecha_retorno_paq:{
          required:true
        },
        dias_paq:{
          required:true,
          digits:true
        },
        precio_paq:{
          required:true,
          digits:true
        },
        descripcion_paq:{
          required:true
        },
        foto_cli:{
                required:true,
                accept:"image/*"}
      },
      messages:{
        nombre_paq:{
          required:"Ingrese el nombe del paquete"
        },
        tipo_paq:{
          required:"Seleccione el tipo de paquete"

        },
        origen_paq:{
          required:"Ingrese el nombre del pais de origen",
          letras:"Solo se aceptan letras"
        },
        destino_paq:{
          required:"Ingrese el nombre del pais de destino",
          letras:"Solo se aceptan letras"
        },
       fecha_partida_paq:{
          required:"Seleccione la fecha de partida"
        },
        fecha_retorno_paq:{
          required:"Seleccione la fecha de retorno"
        },
        dias_paq:{
          required:"Ingrese el numero de dias de viaje",
          digits:"Solo se aceptan numeros"
        },
        precio_paq:{
          required:"Ingrese el precio del paquete ",
          digits:"Solo se aceptan numeros"
        },
        descripcion_paq:{
          required:"Ingrese la descripcion del paquete "
        },
        foto_cli:{
          required:"Ingrese una imagen",
                accept:"El archivo debe ser en formato .jpg,.png"}
      }
    });
</script>

<script type="text/javascript">
      $("#foto_cli").fileinput({
        allowedFileExtensions:["jpeg","jpg","png"],
        dropZoneEnabled:true
      });
</script>









<!--  -->
