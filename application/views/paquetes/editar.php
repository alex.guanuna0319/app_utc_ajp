<br>
<div class="container">
<div class="row">

<div class="col-md-12">

<form
action="<?php echo site_url(); ?>/paquetes/procesarActualizacion"
  method="post"
  >
  <input type="hidden" name="id_paq" id="id_paq"
   value="<?php echo $paquete->id_paq; ?>">
    <br>
    <br>
    <label for="">NOMBRE</label>
    <input class="form-control"    value="<?php echo $paquete->nombre_paq; ?>" type="text" name="nombre_paq" id="nombre_paq" placeholder="Por favor ingrese el nombre del paquete">
    <br>
    <br>
    <label for="">TIPO</label>
    <select class="form-control" value="<?php echo $paquete->tipo_paq; ?>" name="tipo_paq" id="tipo_paq">
        <option value="">--Seleccione--</option>
        <option value="VIAJE">VIAJE</option>
        <option value="CRUCERO">CRUCERO</option>
    </select>
    <br>
    <label for="">ORIGEN</label>
    <input class="form-control"     value="<?php echo $paquete->origen_paq; ?>" type="text" name="origen_paq" id="origen_paq" placeholder="Por favor Ingrese el pais de origen">
    <br>
    <br>
    <label for="">DESTINO</label>
    <input class="form-control"     value="<?php echo $paquete->destino_paq; ?>" type="text" name="destino_paq" id="destino_paq" placeholder="Por favor Ingrese el pais de destino">
    <br>
    <br>
    <label for="">FECHA PARTIDA</label>
    <input class="form-control"     value="<?php echo $paquete->fecha_partida_paq; ?>" type="date" name="fecha_partida_paq" id="fecha_partida_paq" placeholder="Por favor Ingrese la fecha de salida">
    <br>
    <br>
    <label for="">FECHA RETORNO</label>
    <input class="form-control"     value="<?php echo $paquete->fecha_retorno_paq; ?>" type="date" name="fecha_retorno_paq" id="fecha_retorno_paq" placeholder="Por favor Ingrese la fecha de retorno">
    <br>
    <br>
    <label for="">DIAS</label>
    <input class="form-control"     value="<?php echo $paquete->dias_paq; ?>" type="number" name="dias_paq" id="dias_paq" placeholder="Por favor Ingrese los dias de viaje">

    <br>
    <br>
    <label for="">PRECIO</label>
    <input class="form-control"     value="<?php echo $paquete->precio_paq; ?>" type="numbrer" name="precio_paq" id="precio_paq" placeholder="Por favor Ingrese el precio del paquete">

    <br>
    <br>
    <label for="">DESCRIPCION</label>
    <input class="form-control"     value="<?php echo $paquete->descripcion_paq; ?>" type="text" name="descripcion_paq" id="descripcion_paq" placeholder="Por favor Ingrese la descripcion del paquete">

    <br>
    <br>
    <label for="">FOTOGRAFIA</label>
    <input type="file" name="foto_paq"
    accept="image/*"
    id="foto_paq"  value="" >
    <br>
    <br>
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;&nbsp;&nbsp;
    <a href="<?php echo site_url(); ?>/paquetes/index"
      class="btn btn-warning">
      <i class="fa fa-times"> </i> CANCELAR
    </a>

</form>
</div>
</div>
</div>

<script type="text/javascript">
   //Activando  el pais seleccionado para el cliente
   $("#tipoi_paq")
   .val("<?php echo $paquete->tipo_paq; ?>");

</script>
