<?php
    class Cliente extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("cliente",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_cli,$datos){
        $this->db->where("id_cli",$id_cli);
        return $this->db->update("cliente",$datos);
      }
      //funcion para sacar el detalle de un cliente
      public function consultarPorId($id_cli){
        $this->db->where("id_cli",$id_cli);
        $this->db->join("paquete","paquete.id_paq=cliente.fk_id_paq");
        $cliente=$this->db->get("cliente");
        if($cliente->num_rows()>0){
          return $cliente->row();//cuando SI hay clientes
        }else{
          return false;//cuando NO hay clientes
        }
      }
      //funcion para consultar todos lo clientes
      public function consultarTodos(){
        $this->db->join("paquete","paquete.id_paq=cliente.fk_id_paq");
          $listadoClientes=$this->db->get("cliente");
          if($listadoClientes->num_rows()>0){
            return $listadoClientes;//cuando SI hay clientes
          }else{
            return false;//cuando NO hay clientes
          }
      }

      public function eliminar($id_cli){
        $this->db->where("id_cli",$id_cli);
        return $this->db->delete("cliente");
      }


   }//cierre de la clase



   //
 ?>
