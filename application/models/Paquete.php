<?php
    class Paquete extends CI_Model{
      public function __construct(){
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("paquete",$datos);
      }
      //funcion para actualizar
      public function actualizar($id_paq,$datos){
        $this->db->where("id_paq",$id_paq);
        return $this->db->update("paquete",$datos);
      }
      //funcion para sacar el detalle de un paqente
      public function consultarPorId($id_paq){
        $this->db->where("id_paq",$id_paq);
        $paquete=$this->db->get("paquete");
        if($paquete->num_rows()>0){
          return $paquete->row();//cuando SI hay paqentes
        }else{
          return false;//cuando NO hay paqentes
        }
      }
      //funcion para consultar todos lo paqentes
      public function consultarTodos(){
          $listadoPaquetes=$this->db->get("paquete");
          if($listadoPaquetes->num_rows()>0){
            return $listadoPaquetes;//cuando SI hay paqentes
          }else{
            return false;//cuando NO hay paqentes
          }
      }

      public function eliminar($id_paq){
        $this->db->where("id_paq",$id_paq);
        return $this->db->delete("paquete");
      }


   }//cierre de la clase



   //
 ?>
