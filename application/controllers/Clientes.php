<?php
      class Clientes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("cliente");
           $this->load->model("paquete");
            //validando si alguien esta conectado
        }

        public function index(){
          $data["listadoClientes"]=$this->cliente->consultarTodos();
          $this->load->view("header");
          $this->load->view("clientes/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $data["listadoPaquetes"]=$this->paquete->consultarTodos();
          $this->load->view("headerCliente");
          $this->load->view("clientes/nuevo",$data);
          $this->load->view("footerCliente");
        }

        public function editar($id_cli){
          $data["listadoPaquetes"]=$this->paquete->consultarTodos();
          $data["cliente"]=$this->cliente->consultarPorId($id_cli);
          $this->load->view("header");
          $this->load->view("clientes/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_cli=$this->input->post("id_cli");
            $datosClienteEditado=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "sexo_cli"=>$this->input->post("sexo_cli"),
               "fk_id_paq"=>$this->input->post("fk_id_paq")
            );
            if($this->cliente->actualizar($id_cli,$datosClienteEditado)){
                //echo "INSERCION EXITOSA";
                redirect("clientes/index");
            }else{
                echo "ERROR AL ACTUALIZAR";
            }
        }

        public function guardarCliente(){
            $datosNuevoCliente=array(
                "identificacion_cli"=>$this->input->post("identificacion_cli"),
                "apellido_cli"=>$this->input->post("apellido_cli"),
                "nombre_cli"=>$this->input->post("nombre_cli"),
                "telefono_cli"=>$this->input->post("telefono_cli"),
                "email_cli"=>$this->input->post("email_cli"),
                "sexo_cli"=>$this->input->post("sexo_cli"),
              "fk_id_paq"=>$this->input->post("fk_id_paq")
            );
            //Logica de Negocio necesaria para subir la FOTOGRAFIA del cliente
            $this->load->library("upload");//carga de la libreria de subida de archivos
            $nombreTemporal="foto_cliente_".time()."_".rand(1,5000);
            $config["file_name"]=$nombreTemporal;
            $config["upload_path"]=APPPATH.'../uploads/clientes/';
            $config["allowed_types"]="jpeg|jpg|png";
            $config["max_size"]=2*1024; //2MB
            $this->upload->initialize($config);
            //codigo para subir el archivo y guardar el nombre en la BDD
            if($this->upload->do_upload("foto_cli")){
              $dataSubida=$this->upload->data();
              $datosNuevoCliente["foto_cli"]=$dataSubida["file_name"];
            }

            if($this->cliente->insertar($datosNuevoCliente)){
                $this->session->set_flashdata("confirmacion",
                 "Reservacion Exitosa.");
            }else{
               $this->session->set_flashdata("error",
               "Error al procesar, intente nuevamente.");
            }
            redirect("");
        }

        public function procesarEliminacion($id_cli){
             if($this->cliente->eliminar($id_cli)){
                redirect("clientes/index");
            }else{
                echo "ERROR AL ELIMINAR";
            }

        }

    }//cierre de la clase
?>
