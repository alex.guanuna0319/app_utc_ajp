<?php
      class Paquetes extends CI_Controller{
        public function __construct(){
            parent::__construct();
            $this->load->model("paquete");
            //validando si alguien esta conectado
        }

        public function index(){
          $data["listadoPaquetes"]=$this->paquete->consultarTodos();
          $this->load->view("header");
          $this->load->view("paquetes/index",$data);
          $this->load->view("footer");
        }
        public function nuevo(){
          $this->load->view("header");
          $this->load->view("paquetes/nuevo",);
          $this->load->view("footer");
        }

        public function editar($id_paq){
          $data["paquete"]=$this->paquete->consultarPorId($id_paq);
          $this->load->view("header");
          $this->load->view("paquetes/editar",$data);
          $this->load->view("footer");
        }

        public function procesarActualizacion(){
            $id_paq=$this->input->post("id_paq");
            $datosPaqueteEditado=array(

                "nombre_paq"=>$this->input->post("nombre_paq"),
                "tipo_paq"=>$this->input->post("tipo_paq"),
                "origen_paq"=>$this->input->post("origen_paq"),
                "destino_paq"=>$this->input->post("destino_paq"),
                "fecha_partida_paq"=>$this->input->post("fecha_partida_paq"),
                "fecha_retorno_paq"=>$this->input->post("fecha_retorno_paq"),
                "dias_paq"=>$this->input->post("dias_paq"),
                "precio_paq"=>$this->input->post("precio_paq"),
                "descripcion_paq"=>$this->input->post("descripcion_paq")

            );
            if($this->paquete->actualizar($id_paq,$datosPaqueteEditado)){
                //echo "INSERCION EXITOSA";
                redirect("paquetes/index");
            }else{
                echo "ERROR AL ACTUALIZAR";
            }
        }

        public function guardarPaquete(){
            $datosNuevoPaquete=array(
                "nombre_paq"=>$this->input->post("nombre_paq"),
                "tipo_paq"=>$this->input->post("tipo_paq"),
                "origen_paq"=>$this->input->post("origen_paq"),
                "destino_paq"=>$this->input->post("destino_paq"),
                "fecha_partida_paq"=>$this->input->post("fecha_partida_paq"),
                "fecha_retorno_paq"=>$this->input->post("fecha_retorno_paq"),
                "dias_paq"=>$this->input->post("dias_paq"),
                "precio_paq"=>$this->input->post("precio_paq"),
                "descripcion_paq"=>$this->input->post("descripcion_paq")
            );
            //Logica de Negocio necesaria para subir la FOTOGRAFIA del paqente
            $this->load->library("upload");//carga de la libreria de subida de archivos
            $nombreTemporal="foto_paquete_".time()."_".rand(1,5000);
            $config["file_name"]=$nombreTemporal;
            $config["upload_path"]=APPPATH.'../uploads/paquetes/';
            $config["allowed_types"]="jpeg|jpg|png";
            $config["max_size"]=2*1024; //2MB
            $this->upload->initialize($config);
            //codigo para subir el archivo y guardar el nombre en la BDD
            if($this->upload->do_upload("foto_paq")){
              $dataSubida=$this->upload->data();
              $datosNuevoPaquete["foto_paq"]=$dataSubida["file_name"];
            }

            if($this->paquete->insertar($datosNuevoPaquete)){
                $this->session->set_flashdata("confirmacion",
                 "Paquete insertado exitosamente.");
            }else{
               $this->session->set_flashdata("error",
               "Error al procesar, intente nuevamente.");
            }
            redirect("paquetes/index");
        }

        public function procesarEliminacion($id_paq){

            if($this->paquete->eliminar($id_paq)){
                redirect("paquetes/index");
            }else{
                echo "ERROR AL ELIMINAR";
            }
          }
        

    }//cierre de la clase
?>
